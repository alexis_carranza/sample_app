# Module gives us a way to package together related methods, which can be mixed in to Ruby classes using include. In the case of a helper module Rails handles the inclusion for us. Available in all our views.
module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
